FROM redocly/redoc

COPY ./spec/ /usr/share/nginx/html/spec/

ENV SPEC_URL=spec/openapi.yaml
ENV PAGE_TITLE="OneLife API"

EXPOSE 80