# OneLife API

## Goals

To set a API Specification with best practices so that we developers can follow and comply to.

- ADR
- API Specification

## Architecture Decision Records (ADR)

(TBD)

## Run

### API Documentation

To run API Specification server, run the following command.

```bash
# Without Docker Compose
sh ./scripts/start-spec.sh

# With Docker Compose
docker-compose up onelife-api-spec
```

Open <http://localhost:8080> in any brower.  

![ALt text](./docs/API_Spec_Page.png)

### API Mock Server

To run API Mock server, run the following command.

```bash
docker-compose up onelife-api-mock
```

Mock server will run at <http://localhost:8888>. To test if it is functional. Run the following command.

```bash
curl -i http://localhost:8888/v1/cards?partnerCode=molestiae&storeId=earum&deviceId=laborum&signature=consequuntur
```

## Public Pages

The documentation is publicly available at <https://seedcom-developers.gitlab.io/onelife/onelife-api/>.

## References

- OpenAPI 3.0: <https://spec.openapis.org/oas/v3.1.0>
- Swagger: <https://swagger.io/>
- Redoc: <https://redoc.com>
- ADR: <https://adr.github.io/>
