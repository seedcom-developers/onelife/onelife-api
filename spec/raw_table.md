Code|Mô tả
|-|-|
0|Lỗi không xác định. Vui lòng liên hệ support@onelife.vn để được hỗ trợ
2000|Payload thiếu các trường yêu cầu
2001|Payload không chính xác
2002|Chữ ký không chính xác
3000|Quyền truy cập bị từ chối
3001|Tài khoản chưa xác thực
3002|Tài khoản không hoạt động
3003|Mã OTP không chính xác
3004|Quá giới hạn số lần gửi OTP. Vui lòng thử lại sau
4000|Hệ thống đang bảo trì. Vui lòng thử lại sau
4001|Lỗi hệ thống. Vui lòng liên hệ support@onelife.vn để được hỗ trợ
4002|Deadline exceeded
4003|Canceled
4004|Aborted
5001|Tài khoản không tồn tại
6001|Mã thanh toán không chính xác
6002|Mã thanh toán đã hết hạn
6003|Số dư tài khoản không đủ
6004|Giao dịch đã hết hạn
6005|Tài khoản vượt quá hạn mức