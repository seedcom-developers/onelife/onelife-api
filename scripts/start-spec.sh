#!/bin/sh

docker build --no-cache -t onelife_api_spec .
echo "Server is running at http://localhost:8080"
docker run -it \
    -e "SPEC_URL=spec/openapi.yaml" \
    -e "PAGE_TITLE=OneLife API" \
    -v $(pwd)/spec/:/usr/share/nginx/html/spec/ \
    -v $(pwd)/public/assets/:/usr/share/nginx/html/assets/ \
    -p 8080:80 \
    onelife_api_spec
