#!/bin/sh

export OAS_V3_PATH=$(pwd)/spec/openapi.yaml
export OAS_V2_PATH=$(pwd)/spec/swagger.yaml

pip install --upgrade apimatic-cli
npm i -g @redocly/cli@latest

export API_KEY=lKfTMHyBBIPf1_6-P6mgvxvnATmyePD5QqbqdTGr8GtkqRZm1CTZroun85OIo1-o


redocly bundle --output $(pwd)/spec/openapi.json --ext json $OAS_V3_PATH


apimatic-cli transform fromauthkey \
    --auth-key $API_KEY \
    --file $(pwd)/spec/openapi.json \
    --format SwaggerYaml \
    --download-as $OAS_V2_PATH
